# Installer Ansible

## Via le gestionnaire de paquets

- CentOS :

```bash
yum install epel-release
yum update
yum install ansible
```

- Ubuntu

```bash
apt install software-properties-common
apt-add-repository ppa:ansible/ansible
apt update
apt install ansible
```

- Debian 8/9

```bash
echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main" > /etc/apt/sources.list.d/ansible.list
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 93C4A3FD7BB9C367
apt update
apt install ansible
```

## Via le gestionnaire de paquets Python (PyPi)

On peut aussi installer Ansible via pip, mais cette méthode ne crée pas les fichiers de configuration dans /etc/ansible

```bash
apt install python3-pip
python3 -m pip install ansible
```
