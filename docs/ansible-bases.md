# Premiers pas dans Ansible

## Configurer la connexion SSH

1. Par mot de passe

Il faudra indiquer à Ansible qu’on utilise un mot de passe (car il utilise les clés par défaut). Pour cela il faut auparavant installer sshpass :

```bash
apt install sshpass
```

Et on peut ensuite exécuter une commande en direct sur plusieurs serveurs (option -a), en indiquant l’utilisateur avec -u et la saisie d’un mot de passe avec -k.

2. Authentification par clé privée

Générer un couple clé privée/clé publique, avec l'algorithme de chiffrement RSA (en 2048 bits par défaut) : id_rsa (clé privée) et id_rsa.pub (clé publique)

```bash
ssh-keygen -t rsa -b 4096
```

Choisir le répertoire où seront enregistrées les clés
Ne pas entrer de passphrase si vous voulez que la connexion s'effectue automatique (entre 2 serveurs par exemple)

On va ensuite  copier la clé publique qui vient d'être générée (dans le répertoire /root/.ssh ou dans /home/user/.ssh en fonction de l'utilisateur qui a lancé la commande), sur le serveur que l'on souhaite administrer :

```bash
ssh-copy-id -i /home/ansible/.ssh/id_rsa.pub ansible@serveur.mon.domaine
```

Cette commande copie la clé publique du client dans le fichier /home/ansible/.ssh/authorized_keys qui se trouve sur le serveur.

Si on lance la commande ansible avec l’utilisateur "de base", c'est la clé privée de cet utilisateur qui sera utilisée. On peut aussi préciser à Ansible quelle clé utiliser, si on utilise une clé commune à tous les administrateurs par exemple :  dans le fichier /etc/ansible/ansible.cfg, modifier la ligne 107 :

```bash
private_key_file = /srv/devops/keys/id_rsa
```

(le fichier /etc/ansible/ansible.cfg se crée quand on installe ansible avec le gestionnaire de paquets, mais pas avec pip)

## Inventory : paramétrer les serveurs à contrôler

Le fichier par défaut où l’on indique les machines sur lesquelles ansible agira doit être créé dans /etc/ansible. Il se nomme hosts et contient l’inventory.

```bash
vim /etc/ansible/hosts

    [webservers]
    web1 ansible_host=172.16.46.11
    web2 ansible_host=172.16.46.12

    [webservers:vars]
    ansible_user=user
    ansible_password=P@ssword
```

Ce fichier permet de regrouper des serveurs par catégorie, et d’indiquer quelques informations sur ceux-ci. On peut y ajouter des variables. On pourra ensuite utiliser dans ansible les noms des serveurs ou les noms des groupes (ou le mot-clé all pour exécuter des commandes sur tous les serveurs de l’inventaire).

Plus d'information : https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html

## Premières commandes ad-hoc

On peut lancer directement des actions depuis le terminal en utilisant la commande ansible. Pour cela on s’appuiera sur des modules. Exemple avec le module setup, qui récolte un grand nombre d’information sur une machine :

```bash
[ansible@control /srv/ansible] ansible webservers -u ansible -k -m setup
SSH passwords:
```

## Installer des services : les playbooks

Ansible utilise le format YAML pour les playbooks, c’est un format proche du JSON mais qui utilise l’indentation, les tirets et les ":" pour la mise en forme. Cf. http://docs.ansible.com/ansible/latest/YAMLSyntax.html


Voici un exemple de playbook qui installe apache2 sur un serveur :

```bash
vim install_apache2_playbook.yml
	---
	- hosts: apache
  	  become: yes
  	  tasks:
    	- name: install apache2
      	  apt: 
            name: apache2
            update_cache: yes
            state: latest
```

Il faut bien respecter la mise en forme : - pour les listes de taches, des espaces pour décaler les options (le yaml est très chatouilleux).
L'option become: yes indique qu'on souhaite utiliser sudo, et donc il faut demander le mot de passe pour sudo lors de l'exécution du playbook (avec l'option -K) . Pour le lancer on utilise la commande ansible-playbook :

```bash
ansible-playbook -K install_apache2_playbook.yml
```

Un playbook comprend un ou plusieur plays qui contiennet une ou plusieurs tasks, suivant le schéma suivant :

![Structure d'un playbook](playbook-structure.png)

Chaque play nécessite de configurer certains paramètres (hosts, variables), et peut contenir, outre des tasks, des roles (ensembles de tâches prédéfinies) et des handlers, qui se déclenchent seulement quand ils sont appelés (un peu à la manière de fonctions dans un script). Ci-dessous un exemple regroupant ces éléments :

![Exemple de play](play-example.png)

## Utiliser les variables

Les variables s'utilisent comme dans des scripts. Elles peuvent se déclarer dans l'inventaire et les fichiers s'y rapportant, ou directement dans les playbooks.

![Exemples de variables d'inventaire](inventory-vars.png)

On peut utiliser les variables comme conditions, y compris les variables récupérées grâce à l'étape Gathering Facts :

```yaml
tasks:
- name: Install apache
  apt:
    name: {{ item }}
    state: latest
  with_items:
    - apache2
  when: ansible_distribution == 'Debian' or ansible_distribution == 'Ubuntu'
- name: Install httpd
  yum:
    name: {{ item }}
    state: latest
  with_items:
    - httpd
  when: ansible_distribution == 'Red Hat Enterprise Linux'
```

On peut comme dans un script shell demander des informations à l'utilisateur qui lance le playbook : 

```bash
---
- hosts: all
  vars_prompt:

    - name: username
      prompt: "What is your username?"
      private: no

    - name: password
      prompt: "What is your password?"

  tasks:

    - debug:
        msg: 'Logging in as {{ username }}'
```

## Sécuriser les informations avec Vault

Pour chiffrer les données qui sont passées en paramètres aux playbooks et commandes Ansible, on peut utiliser ansible-vault.
On crée un fichier contenant le mot de passe à utiliser avec Vault : 

```bash
echo 'P@ssword' > /etc/ansible/.vaultpass
chmod 400 /etc/ansible/.vaultpass 
# (attention à l'utilisateur qui se servira de ce fichier, il faut qu'il ait les droits)
```

On pourra utiliser ce fichier pour chiffrer l'inventaire par exemple :

```bash
ansible-vault encrypt --vaut-id /etc/ansible/.vaultpass /etc/ansible/hosts
```

Pour éditer l'inventaire, il faudra utiliser :

```bash
ansible-vault edit --vaut-id /etc/ansible/.vaultpass /etc/ansible/hosts
```

Pour l'utiliser il faudra indiquer le mot de passe dans les commandes :

```bash
ansible --vault-id /etc/ansible/.vaultpass webservers -m setup
```

On peut aussi utiliser un mot de passe à taper à chaque utilisation de vault, dans ce cas on utilise --ask-vault-pass à la place de --vault-id

## Utiliser les templates Jinja2

Pour créer des fichiers de configuration, il est intéressant d'utiliser des templates Jinja2 qui sont simplement des fichiers textes suffixés avec .j2, à l'intérieur desquels on a remplacé les valeurs qui nous intéressent par des variables entourées de {{ }}.

Voici un exemple de fichier Apache :

![Fichier Apache d'exemple](template-example.png)
