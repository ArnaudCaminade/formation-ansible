# Configurer les serveurs Windows

Ansible utilise WinRM pour la connexion à distance aux serveurs Windows. 
Sur le serveur Windows, il faut lancer le script ConfigureRemotingforAnsible.ps1 qui active tout le nécessaire sur le serveur Windows. Il est disponible à l'adresse suivante : https://github.com/ansible/ansible/blob/devel/examples/scripts/ConfigureRemotingForAnsible.ps1
(il faut bien sûr l'adapter selon les règles de l'entreprise, plus d'informations ici : https://docs.ansible.com/ansible/latest/user_guide/windows_setup.html )
Il faudra notamment autoriser WinRM dans le pare-feu (les règles dans le script Powershell sont en anglais, donc la commande ne passe pas) :

```powershell
netsh advfirewall firewall add rule Profile=Domain name="Autoriser WinRM HTTPS" dir=in localport=5986 protocol=TCP action=allow
```

## Configurer Ansible pour accéder à Windows

Sur la machine Ansible, il faut installer pip puis pywinrm :

```bash
sudo yum/apt install python-pip
sudo python/python3 -m pip install pywinrm 
```

(utilizer python3 si l'installation d'Ansible a été faite avec python3 et pip)

On crée ensuite un hôte dans l'inventory.

```bash
vim /etc/ansible/hosts
	[win]
	192.168.46.21
```

Et on crée des variables pour ce groupe de machines :

```bash
mkdir /etc/ansible/group_vars
vim /etc/ansible/group_vars/win.yml
    ansible_user=Administrateur
    ansible_password=P@ssword
    ansible_port=5986
    ansible_connection=winrm
    ansible_winrm_server_cert_validation=ignore
```

Le dossier group_vars doit se trouver dans le même dossier que le fichier inventory, et les fichiers yaml doivent être nommés comme les groupes dans le fichier inventory (ici [win] --> group_vars/win.yml).

Il est bien sûr recommandé de protéger ce fichier avec ansible-vault.

Il ne reste plus qu'à s'amuser avec les modules Windows : http://docs.ansible.com/ansible/latest/list_of_windows_modules.html

Une ressource intéressante : https://www.it-connect.fr/debutez-avec-ansible-et-gerez-vos-serveurs-windows/